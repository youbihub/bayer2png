#include "bayer2png/bayer2png.h"

#include <iostream>
#include <vector>

#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"

namespace bay2png {
  void Bayer2Png(const std::filesystem::path& input_folder,
                 const std::filesystem::path& output_folder) {
    const auto image_path = [&]() {
      auto image_path{std::vector<std::filesystem::path>{}};
      for (auto& p : std::filesystem::directory_iterator(input_folder)) {
        image_path.push_back(p.path());
      }
      return image_path;
    }();

    if (!std::filesystem::exists(output_folder)) {
      std::cout << "Output Directory does not exist yet..";
      std::filesystem::create_directory(output_folder);
      std::cout << " Created directory!" << std::endl;
    }

    for (double k = 0.; auto&& p : image_path) {
      auto outpath{output_folder / p.filename().replace_extension("png")};
      std::cout << int(k++ / image_path.size() * 100) << "%\t" << outpath << std::endl;
      const auto im{cv::imread(p, cv::IMREAD_GRAYSCALE)};
      auto out{cv::Mat(im.rows, im.cols, CV_8UC3)};
      cv::cvtColor(im, out, cv::COLOR_BayerRG2BGR_EA);
      cv::imwrite(outpath, out);
    }
    std::cout << "100%\t"
              << "Folder successfully converted" << std::endl;

    return;
  }

}  // namespace bay2png
