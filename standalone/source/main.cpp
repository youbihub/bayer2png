#include <bayer2png/bayer2png.h>
#include <bayer2png/version.h>

#include <iostream>
#include <string>
#include <unordered_map>

auto main(int argc, char** argv) -> int {
  if (argc != 3) {
    std::cout << "Function usage: Bayer2Png <input_folder> <output_folder>" << std::endl;
    return 0;
  }

  bay2png::Bayer2Png(argv[1], argv[2]);
  return 0;
}
