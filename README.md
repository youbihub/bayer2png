Convert folder of bayerRG raw images to pngs

usage:

Bayer2Png input_folder output_folder


Converted images are about ~20%-25% larger than bayerRG
