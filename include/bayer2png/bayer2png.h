#pragma once
#include <filesystem>

namespace bay2png {
  void Bayer2Png(const std::filesystem::path& input_folder,
                 const std::filesystem::path& output_folder);
}  // namespace bay2png
